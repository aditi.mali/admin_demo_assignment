package adminDemoPkg;

import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class AdminDemo {
	
	public WebDriver  driver;
  
  @BeforeClass
  public void beforeClass() {
	  
		driver= new ChromeDriver();
		driver.get(" https://admin-demo.nopcommerce.com/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	}
  
  @Test(priority=1)
  public void Login()
  {
	  
	 // driver.findElement(By.id("Email")).sendKeys("");
	//  driver.findElement(By.id("Password")).sendKeys("");
	  //driver.findElement(By.xpath("//button[@type='submit']")).click();
	  //driver.findElement(By.xpath("(//*[@class='nav-link'])[4]")).click();
	  
	  
	  WebElement email = driver.findElement(By.id("Email"));
	     email.clear();
	     email.sendKeys("admin@yourstore.com");
	     
	      WebElement pass = driver.findElement(By.id("Password"));
	      pass.clear();
	      pass.sendKeys("admin");
	      
	      driver.findElement(By.xpath("//*[@class='button-1 login-button']")).click();
	   
	      
	      String actual = driver.findElement(By.partialLinkText("John Smith")).getText();
	      System.out.println(actual);
	      String excepted = "John Smith";
	      Assert.assertEquals(actual, excepted);
	      driver.findElement(By.xpath("(//*[@class='nav-link'])[4]")).click();
  }
  @Test(priority=2)
  public void Categories() throws Exception 
  {
	  
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//li//a[@href='/Admin/Category/List']")).click();
	  
	   driver.findElement(By.xpath("//i[@class='fas fa-plus-square']")).click();
	      
	      Thread.sleep(3000);
	      
	      File f=new File("/home/aditi.mali/Documents/addToCategory1.xls");
			FileInputStream fis= new FileInputStream(f);		
			Workbook book=Workbook.getWorkbook(fis);
			Sheet sh=book.getSheet("Sheet1");
			
			int rowCount= sh.getRows();
			int columnsCount=sh.getColumns();
			for(int i=1; i<rowCount; i++)
			{
				String name= sh.getCell(0,1).getContents();
				
				String description= sh.getCell(1,1).getContents();
				
				String pricefrom= sh.getCell(2,1).getContents();
				
				String priceto= sh.getCell(3,1).getContents();
				
				String displayorder= sh.getCell(4,1).getContents();			
				driver.findElement(By.xpath("//input[@name='Name']")).sendKeys(name);
				 Thread.sleep(1000);
				driver.switchTo().frame(0);
				driver.findElement(By.id("Description_ifr")).sendKeys(description);
				 Thread.sleep(1000);
				JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
				jsExecutor.executeScript("document.getElementById('nestedSetting106772939').scrollIntoView()");
				
				driver.findElement(By.xpath("(//input[@role='spinbutton'])[3]")).sendKeys(pricefrom);
				
				driver.findElement(By.xpath("(//input[@role='spinbutton'])[5]")).sendKeys(priceto);
				
				driver.findElement(By.xpath("(//input[@role='spinbutton'])[7]")).sendKeys(displayorder); 
		}
	      
	  
	 
  }
 
  
  @Test(priority=3)
  public void Products() 
  {
	  driver.findElement(By.partialLinkText("Products")).click();
	  driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
	  WebElement testDropDown = driver.findElement(By.id("SearchCategoryId"));
	  Select dropdown = new Select(testDropDown);
	  dropdown.selectByIndex(2);
	  driver.findElement(By.id("search-products")).click();
	 
	 
	 
  }
  @Test(priority=4)
  public void Manufactures() throws Exception, IOException 
  {

	  driver.findElement(By.partialLinkText("Manufacturers")).click();
	  driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();

	  File f=new File("/home/aditi.mali/Documents/addToCategory1.xls");
	  FileInputStream fis= new FileInputStream(f);

	  Workbook book=Workbook.getWorkbook(fis);
	  Sheet sh=book.getSheet("Sheet2");

	  int rowCount= sh.getRows();
	  int columnsCount=sh.getColumns();
	  for(int i=1; i<rowCount; i++)
	  {
		  String name= sh.getCell(0,1).getContents();
		  String description= sh.getCell(1,1).getContents();
		  driver.findElement(By.id("Name")).sendKeys(name);
		  driver.switchTo().frame(0);

		  driver.findElement(By.id("tinymce")).sendKeys(description);
		  driver.findElement(By.xpath("//button[@name='save']")).click();

	  }

	  
  }
  @Test(priority=5)
  public void Logout() 
  {
	  driver.findElement(By.xpath("//a[text()='Logout']")).click();
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
